package com.jsp.serv;

import com.jsp.database.Famous;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/famous")
public class FamousServ extends HttpServlet {
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");

        String option = req.getParameter("type");
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String info = req.getParameter("info");
        String stars = req.getParameter("stars");
        String photoURL = req.getParameter("photo-url");
        String key = req.getParameter("key");

        switch (option) {
            case "insert":
                Famous.insert(name, info, Integer.parseInt(stars), photoURL);
                break;

            case "update":
                if (!Famous.update(Integer.parseInt(id), name, info, Integer.parseInt(stars), photoURL))
                    throw new ServletException("UPDATE FAILED");
                break;
            case "delete":
                if (!Famous.delete(Integer.parseInt(id)))
                    throw new ServletException("DELETE FAILED");
                break;
        }

        if (option.equals("search")) {
            var list = Famous.search(key);
            if (list.isEmpty())
                throw new ServletException("SEARCH FAILED");
            else {
                resp.setContentType("text/html");

                // Hello
                PrintWriter out = resp.getWriter();
                out.println("<html><body>");
                for (Famous famous : list) {
                    out.println("<h1>" + famous.getId()+"</h1>");
                    out.println("<h1>" + famous.getName()+"</h1>");
                    out.println("<h1>" + famous.getInfo()+"</h1>");
                    out.println("<h1>" + famous.getStars()+"</h1>");
                    out.println("<h1>" + famous.getPhotoURL()+"</h1>");
                    out.println("<a href='frame-famous.jsp'>返回家乡景点</a>");
                }
                out.println("</body></html>");
            }
        } else {
            resp.sendRedirect("/frame-famous.jsp");
        }

    }
}
