package com.jsp.serv;

import com.jsp.database.Culture;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/culture")
public class CultureServ extends HttpServlet {
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");

        String option = req.getParameter("type");
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String info = req.getParameter("info");
        String his = req.getParameter("his");
        String photoURL = req.getParameter("photo-url");
        String key = req.getParameter("key");

        switch (option) {
            case "insert":
                Culture.insert(name, info, his, photoURL);
                break;
            case "update":
                if (!Culture.update(Integer.parseInt(id), name, info,his, photoURL))
                    throw new ServletException("UPDATE FAILED");
                break;
            case "delete":
                if (!Culture.delete(Integer.parseInt(id)))
                    throw new ServletException("DELETE FAILED");
                break;
        }
        if (option.equals("search")) {
            var list = Culture.search(key);
            if (list.isEmpty())
                throw new ServletException("SEARCH FAILED");
            else {
                resp.setContentType("text/html");

                // Hello
                PrintWriter out = resp.getWriter();
                out.println("<html><body>");
                for (Culture Culture : list) {
                    out.println("<h1>" + Culture.getId()+"</h1>");
                    out.println("<h1>" + Culture.getName()+"</h1>");
                    out.println("<h1>" + Culture.getInfo()+"</h1>");
                    out.println("<h1>" + Culture.getHis()+"</h1>");
                    out.println("<h1>" + Culture.getPhotoURL()+"</h1>");
                    out.println("<a href='frame-culture.jsp'>返回家乡文化</a>");
                }
                out.println("</body></html>");
            }
        } else {
            resp.sendRedirect("/frame-culture.jsp");
        }
    }
}
