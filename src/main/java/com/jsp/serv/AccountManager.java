package com.jsp.serv;

import com.jsp.database.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet("/account")
public class AccountManager extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");

        String account = req.getParameter("account");
        String pwd = req.getParameter("pwd");
        String name = req.getParameter("name");
        String phone = req.getParameter("phone");
        String photoURL = req.getParameter("photo-url");

        User.register(Integer.parseInt(account), pwd, name, phone, photoURL);
        resp.sendRedirect("/login.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");

        String name = req.getParameter("name");
        String password = req.getParameter("pwd");

        if (User.login(name, password)) {
            resp.sendRedirect("frame-dishes.jsp");
        } else {
            resp.sendRedirect("login.jsp");
        }
    }
}
