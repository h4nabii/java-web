package com.jsp.serv;

import com.jsp.database.Culture;
import com.jsp.database.Dishes;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/dishes")
public class DishesServ extends HttpServlet {
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");

        String option = req.getParameter("type");
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String info = req.getParameter("info");
        String taste = req.getParameter("taste");
        String photoURL = req.getParameter("photo-url");
        String key = req.getParameter("key");

        switch (option) {
            case "insert":
                Dishes.insert(name, taste, info, photoURL);
                break;
            case "update":
                if (!Dishes.update(Integer.parseInt(id), name, taste, info, photoURL))
                    throw new ServletException("UPDATE FAILED");
                break;
            case "delete":
                if (!Dishes.delete(Integer.parseInt(id)))
                    throw new ServletException("DELETE FAILED");
                break;
        }
        if (option.equals("search")) {
            var list = Dishes.search(key);
            if (list.isEmpty())
                throw new ServletException("SEARCH FAILED");
            else {
                resp.setContentType("text/html");

                // Hello
                PrintWriter out = resp.getWriter();
                out.println("<html><body>");
                for (Dishes Dishes : list) {
                    out.println("<h1>" + Dishes.getId()+"</h1>");
                    out.println("<h1>" + Dishes.getName()+"</h1>");
                    out.println("<h1>" + Dishes.getInfo()+"</h1>");
                    out.println("<h1>" + Dishes.getTaste()+"</h1>");
                    out.println("<h1>" + Dishes.getPhotoURL()+"</h1>");
                    out.println("<a href='frame-dishes.jsp'>返回家乡美食</a>");
                }
                out.println("</body></html>");
            }
        } else {
            resp.sendRedirect("/frame-dishes.jsp");
        }
    }
}
