package com.jsp.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class User {
    private final int id;
    private final String pwd;
    private final String name;
    private final String phone;
    private final String photoURL;

    public User(int id, String name, String pwd, String phone, String photoURL) {
        this.id = id;
        this.name = name;
        this.pwd = pwd;
        this.phone = phone;
        this.photoURL = photoURL;
    }

    public int getId() {
        return id;
    }

    public String getPwd() {
        return pwd;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public static void register(int user, String pwd, String name, String phone, String photoURL) {
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            sql.executeUpdate(
                    "insert into register values (" +
                            "null, " +
                            "'" + name + "', " +
                            "'" + pwd + "', " +
                            "'" + phone + "', " +
                            "'" + photoURL + "');"
            );

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean login(String user, String pwd) {
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            ResultSet rs = sql.executeQuery(
                    "select * from register " +
                            "where name = '" + user + "' " +
                            "and password = '" + pwd + "';"
            );
            return rs.next();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<User> getAll() {
        var users = new ArrayList<User>();
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            ResultSet rs = sql.executeQuery("select * from register;");
            while (rs.next()) {
                users.add(new User(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)
                ));
            }
            return users;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
