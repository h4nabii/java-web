package com.jsp.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Culture {
    private final int id;
    private final String name;
    private final String info;
    private final String his;
    private final String photoURL;

    public Culture(int id, String name, String info, String his, String photoURL) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.his = his;
        this.photoURL = photoURL;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public String getHis() {
        return his;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public static void insert(String name, String info, String his, String photoURL) {
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            int count = sql.executeUpdate(
                    "insert into homeculture values (" +
                            "null, " +
                            "'" + name + "', " +
                            "'" + info + "', " +
                            "'" + his + "', " +
                            "'" + photoURL + "');"
            );

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean delete(int id) {
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            int count = sql.executeUpdate(
                    "delete from homeculture where cultureid=" + id + ";"
            );
            return count > 0;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean update(int id, String name, String info, String his, String photoURL) {
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            int count = sql.executeUpdate(
                    "update homeculture set " +
                            "culturename='" + name + "', " +
                            "cultureinfo='" + info + "', " +
                            "culturehis='" + his + "', " +
                            "culturephoto='" + photoURL + "' " +
                            "where cultureid=" + id + ";"
            );
            return count > 0;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Culture> search(String str) {
        var search = new ArrayList<Culture>();
        try (Connection con = DBConnector.getConnection()) {
            int id;
            try {
                id = Integer.parseInt(str);
            } catch (NumberFormatException e) {
                id = -1;
            }
            Statement sql = con.createStatement();
            ResultSet rs = sql.executeQuery(
                    "select * from homeculture " +
                            "where (cultureid=" + id + ") or " +
                            "(culturename like '*" + str + "*') or " +
                            "(cultureinfo like '*" + str + "*') or " +
                            "(culturehis like '*" + str + "*') or " +
                            "(culturephoto like '*" + str + "*');"
            );
            while (rs.next()) {
                search.add(new Culture(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)
                ));
            }
            return search;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Culture> getAll() {
        var famousList = new ArrayList<Culture>();
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            ResultSet rs = sql.executeQuery("select * from homeculture;");
            while (rs.next()) {
                famousList.add(new Culture(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)
                ));
            }
            return famousList;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
