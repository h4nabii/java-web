package com.jsp.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Dishes {

    private final int id;
    private final String name;
    private final String taste;
    private final String info;
    private final String photoURL;

    public Dishes(int id, String name, String taste, String info, String photoURL) {
        this.id = id;
        this.name = name;
        this.taste = taste;
        this.info = info;
        this.photoURL = photoURL;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTaste() {
        return taste;
    }

    public String getInfo() {
        return info;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public static void insert(String name, String taste, String info, String photoURL) {
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            int count = sql.executeUpdate(
                    "insert into homedishes values (" +
                            "null, " +
                            "'" + name + "', " +
                            "'" + taste + "', " +
                            "'" + info + "', " +
                            "'" + photoURL + "');"
            );

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean delete(int id) {
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            int count = sql.executeUpdate(
                    "delete from homedishes where foodid=" + id + ";"
            );
            return count > 0;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean update(int id, String name, String taste, String info, String photoURL) {
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            int count = sql.executeUpdate(
                    "update homedishes set " +
                            "foodname='" + name + "', " +
                            "foodtaste='" + taste + "', " +
                            "foodinfo='" + info + "', " +
                            "foodphoto='" + photoURL + "' " +
                            "where foodid=" + id + ";"
            );
            return count > 0;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Dishes> search(String str) {
        var search = new ArrayList<Dishes>();
        try (Connection con = DBConnector.getConnection()) {
            int id;
            try {
                id = Integer.parseInt(str);
            } catch (NumberFormatException e) {
                id = -1;
            }
            Statement sql = con.createStatement();
            ResultSet rs = sql.executeQuery(
                    "select * from homedishes " +
                            "where (foodid=" + id + ") or " +
                            "(foodname like '*" + str + "*') or " +
                            "(foodinfo like '*" + str + "*') or " +
                            "(foodtaste like '*" + str + "*') or " +
                            "(foodphoto like '*" + str + "*');"
            );
            while (rs.next()) {
                search.add(new Dishes(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)
                ));
            }
            return search;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Dishes> getAll() {
        var famousList = new ArrayList<Dishes>();
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            ResultSet rs = sql.executeQuery("select * from homedishes;");
            while (rs.next()) {
                famousList.add(new Dishes(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)
                ));
            }
            return famousList;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
