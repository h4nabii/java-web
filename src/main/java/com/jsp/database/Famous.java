package com.jsp.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Famous {
    private final int id;
    private final String name;
    private final String info;
    private final int stars;
    private final String photoURL;

    public Famous(int id, String name, String info, int stars, String photoURL) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.stars = stars;
        this.photoURL = photoURL;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public int getStars() {
        return stars;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public static void insert(String name, String info, int stars, String photoURL) {
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            int count = sql.executeUpdate(

                    "insert into homefamous values (" +
                            "null, " +
                            "'" + name + "', " +
                            "'" + info + "', " +
                            stars + ", " +
                            "'" + photoURL + "');"
            );

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean delete(int id) {
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            int count = sql.executeUpdate(
                    "delete from homefamous where famousid=" + id + ";"
            );
            return count > 0;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean update(int id, String name, String info, int stars, String photoURL) {
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            int count = sql.executeUpdate(
                    "update homefamous set " +
                            "famousname='" + name + "', " +
                            "famousinfo='" + info + "', " +
                            "famousstars=" + stars + ", " +
                            "famousphoto='" + photoURL + "' " +
                            "where famousid=" + id + ";"
            );
            return count > 0;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Famous> search(String str) {
        var search = new ArrayList<Famous>();
        try (Connection con = DBConnector.getConnection()) {
            int num;
            try {
                num = Integer.parseInt(str);
            } catch (NumberFormatException e) {
                num = -1;
            }
            Statement sql = con.createStatement();
            ResultSet rs = sql.executeQuery(
                    "select * from homefamous " +
                            "where (famousid=" + num + ") or " +
                            "(famousname like '*" + str + "*') or " +
                            "(famousinfo like '*" + str + "*') or " +
                            "(famousstars=" + num + ") or " +
                            "(famousphoto like '*" + str + "*');"
            );
            while (rs.next()) {
                search.add(new Famous(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5)
                ));
            }
            return search;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Famous> getAll() {
        var famousList = new ArrayList<Famous>();
        try (Connection con = DBConnector.getConnection()) {
            Statement sql = con.createStatement();
            ResultSet rs = sql.executeQuery("select * from homefamous;");
            while (rs.next()) {
                famousList.add(new Famous(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5)
                ));
            }
            return famousList;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
