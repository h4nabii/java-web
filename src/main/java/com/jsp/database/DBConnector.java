package com.jsp.database;

import javax.naming.*;
import javax.sql.*;
import java.sql.Connection;
import java.sql.SQLException;

public class DBConnector {
    private static DataSource ds = null;

    static {
        try {
            Context context = new InitialContext();
            Context contextNeeded = (Context) context.lookup("java:comp/env");
            ds = (DataSource) contextNeeded.lookup("ljc");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
