<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/6/15
  Time: 13:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.jsp.database.User" %>
<html>
<head>
    <link rel="stylesheet" href="css/frames.css">
    <title></title>
</head>
<body>
<div class=container>
    <table class=data>
        <tr class=header>
            <th class=space>ID</th>
            <th class=space>名称</th>
            <th class=space>密码</th>
            <th class=space>电话</th>
            <th class=space>照片</th>
        </tr>
        <%
            var users = User.getAll();
            for (var user : users) {
                out.print("<tr>");
                out.print("<td class=space>" + user.getId() + "</td>");
                out.print("<td class=space>" + user.getName() + "</td>");
                out.print("<td class=space>" + user.getPwd() + "</td>");
                out.print("<td class=space>" + user.getPhone() + "</td>");
                out.print("<td class='space space5'>" + "<img src='"+user.getPhotoURL()+"' style='float:left;height:100px;border: 2px black solid;'>" + "</td>");
                out.print("</tr>");
            }
        %>
    </table>
    <a class="return-login" href="login.jsp">返回登录界面</a>
</div>
</body>
</html>
