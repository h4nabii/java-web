<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/6/15
  Time: 0:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/log-reg.css">
    <title></title>
</head>

<body>
<form action="${pageContext.request.contextPath}/account" method="post">
    <fieldset>
        <legend>用户注册</legend>
        <table>
            <tr>
                <td><label for="account">账号：</label></td>
                <td><input type="text" name="account" id="account"/></td>
            </tr>
            <tr>
                <td><label for="pwd">密码：</label></td>
                <td><input type="password" name="pwd" id="pwd"/></td>
            </tr>
            <tr>
                <td><label for="pwdCek">确认密码：</label></td>
                <td><input type="password" name="pwdCek" id="pwdCek"/></td>
            </tr>
            <tr>
                <td><label for="name">姓名：</label></td>
                <td><input type="text" name="name" id="name"/></td>
            </tr>
            <tr>
                <td><label for="phone">联系电话：</label></td>
                <td><input type="text" name="phone" id="phone"/></td>
            </tr>
            <tr>
                <td><label for="photo-url">图片url</label></td>
                <td><input type="text" name="photo-url" id="photo-url"/></td>
            </tr>
        </table>
        <p>
            <input type="submit" value="注&emsp;册"/>
        </p>
        <p>
            <a href="login.jsp">已有账号，去登录</a>
        </p>
    </fieldset>
</form>

<script src="lib/jquery/dist/jquery.min.js"></script>
<script src="lib/validation/dist/localization/messages_zh.js"></script>
<script src="lib/validation/dist/jquery.validate.js"></script>
<script type="text/javascript">
    $.validator.addMethod("phone", function (value) {
        return new RegExp(/^(13[0-9]|14[57]|15[0-35-9]|17[6-8]|18[0-9])[0-9]{8}$/).test(value);
    }, "手机号码不正确");

    let form = $("form");
    $(function () {
        form.validate({
            rules: {
                account: {
                    required: true,
                    digits: true,
                    rangelength: [6, 12]
                },
                pwd: {
                    required: true,
                    rangelength: [6, 12]
                },
                pwdCek: {
                    required: true,
                    equalTo: "#pwd"
                },
                name: {
                    required: true,
                    rangelength: [2, 6]
                },
                phone: {
                    required: true,
                    phone: true
                },
                address: {
                    required: true,
                    minlength: [2]
                }
            },
            messages: {
                account: {
                    required: "账号不能为空",
                    digits: "账号必须为数字",
                    rangelength: "账号的长度必须为6到12位"
                },
                pwd: {
                    required: "密码不能为空",
                    rangelength: "密码的长度必须为6到12位"
                },
                pwdCek: {
                    required: "确认密码不能为空",
                    equalTo: "两次密码必须输入一致"
                },
                name: {
                    required: "姓名不能为空",
                    rangelength: "姓名的长度必须为2到6位"
                },
                phone: {
                    required: "手机号码不能为空",
                    phone: "请输入正确的手机号码"
                },
                address: {
                    required: "地址不能为空",
                    minlength: "地址至少大于2位字符"
                }
            }
        });
        form.submit(function () {
            let account = $("#account").val();
            let pwd1 = $("#pwd1").val();
            let pwd2 = $("#pwd2").val();
            let name = $("#name").val();
            let phone = $("#phone").val();
            let address = $("#address").val();
            if (account === "" || pwd1 === "" || pwd2 === "" || name === "" || phone === "" || address === "") {
                return false;
            }
            alert("注册成功")
        });
    });
</script>
</body>
</html>