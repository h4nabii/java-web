<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/6/15
  Time: 13:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.jsp.database.Dishes" %>
<html>
<head>
    <link rel="stylesheet" href="css/frames.css">
    <title></title>

</head>
<body>
<div class=container>
    <form class="form">
        <div>
            <label><input type="radio" name="type" value="insert" checked/>新增</label>
            <label><input type="radio" name="type" value="update"/>修改</label>
            <label><input type="radio" name="type" value="delete"/>删除</label>
        </div>
        <div>
            <label for="id" class="label">ID：</label>
            <input type="text" name="id" id="id"/>
        </div>
        <div>
            <label for="name" class="label">名称：</label>
            <input type="text" name="name" id="name"/>
        </div>
        <div>
            <label for="info" class="label">信息：</label>
            <input type="text" name="info" id="info"/>
        </div>
        <div><label for="taste" class="label">口味：</label>
            <input type="text" name="taste" id="taste"/>
        </div>
        <div>
            <label for="photo-url" class="label">照片：</label>
            <input type="text" name="photo-url" id="photo-url"/>
        </div>
        <div>
            <input type="submit" id="submit" value="提交" formaction="/dishes"/>
        </div>
    </form>
    <form>
        <div>
            <label><input type="radio" name="type" value="search"/>开启功能</label>
        </div>
        <div>
            <label for="key" class="label">搜索：</label>
            <input type="text" name="key" id="key"/>
        </div>
        <div>
            <input type="submit" id="submit2" value="搜索" formaction="/dishes"/>
        </div>
    </form>
    <table class="data">
        <tr class="header">
            <th class="space space1">ID</th>
            <th class="space space2">名称</th>
            <th class="space space3">口味</th>
            <th class="space space4">信息</th>
            <th class="space space5">照片</th>
        </tr>
        <%
            var dishes = Dishes.getAll();
            for (var dish : dishes) {
                out.print("<tr>");
                out.print("<td class='space space1'>" + dish.getId() + "</td>");
                out.print("<td class='space space2'>" + dish.getName() + "</td>");
                out.print("<td class='space space3'>" + dish.getTaste() + "</td>");
                out.print("<td class='space space4'>" + dish.getInfo() + "</td>");
                out.print("<td class='space space5'>" + "<img src='" + dish.getPhotoURL() + "' style='float:left;height:100px;border: 2px black solid;'>" + "</td>");
                out.print("</tr>");
            }
        %>
    </table>
    <a class="return-login" href="login.jsp">返回登录界面</a>
</div>
</body>
</html>
