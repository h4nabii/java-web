<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023/6/15
  Time: 0:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/log-reg.css">
    <title></title>
</head>

<body>
<form action="${pageContext.request.contextPath}/account" method="get">
    <fieldset>
        <legend>用户登录</legend>
        <table>
            <tr>
                <td><label for="name">账号：</label></td>
                <td><input type="text" name="name" id="name"/></td>
                <td><span id="act-text"></span></td>
            </tr>
            <tr>
                <td><label for="pwd">密码：</label></td>
                <td><input type="password" name="pwd" id="pwd"/></td>
                <td><span id="pwd-text"></span></td>
            </tr>
        </table>
        <p>
            <input type="submit" value="登&emsp;录"/>
        </p>
        <p>
            <a href="register.jsp">没有账号，去注册一个</a>
        </p>
        <p>
            <a href="frame-accounts.jsp">查看已有账号</a>
        </p>
    </fieldset>
</form>
</body>
</html>